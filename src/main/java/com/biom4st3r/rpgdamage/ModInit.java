package com.biom4st3r.rpgdamage;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.particle.v1.ParticleFactoryRegistry;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer
{
	public static final String MODID = "rpgdamage";

	@Override
	public void onInitialize() {
		Registry.register(Registry.PARTICLE_TYPE, new Identifier(MODID, "rpg_damage"), RpgDamageEffect.TYPE);
		
		ParticleFactoryRegistry.getInstance().register(RpgDamageEffect.TYPE, (type,world,x,y,z,vx,vy,vz)->
		{
			return new RpgDamageParticle(world, x, y, z, vx, vy, vz, 1, 1, 1, 1337);
		});
	}
	public static CustomPayloadS2CPacket createParticle(float damage,double x, double y, double z,double vx,double vy,double vz)
	{
		PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
		pbb.writeFloat(damage);
		pbb.writeDouble(x);
		pbb.writeDouble(y);
		pbb.writeDouble(z);
		pbb.writeDouble(vx);
		pbb.writeDouble(vy);
		pbb.writeDouble(vz);
		return new CustomPayloadS2CPacket(new Identifier(MODID,"particle_packet"), pbb);
	}
}
