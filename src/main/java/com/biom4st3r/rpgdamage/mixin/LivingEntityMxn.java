package com.biom4st3r.rpgdamage.mixin;

import com.biom4st3r.rpgdamage.ModInit;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMxn extends Entity {

    public LivingEntityMxn(EntityType<?> type, World world) {
        super(type, world);
        // TODO Auto-generated constructor stub
    }

    @Inject(
       at = @At(
          value="INVOKE",
          target="net/minecraft/entity/LivingEntity.getHealth()F",
          ordinal = 0,
          shift = Shift.BEFORE),
       method = "applyDamage",
       cancellable = false,
       locals = LocalCapture.NO_CAPTURE)
    private void applyDamageAndSendParticle(DamageSource source, float amount, CallbackInfo ci)
    {
        World world = ((LivingEntity)(Object)this).world;
        if(!world.isClient)
        {
            for(PlayerEntity player : ((ServerWorld)this.world).getPlayers())
            {
                if(player.getBlockPos().isWithinDistance(new Vec3d(this.getX(), this.getY(), this.getZ()), 32))
                {
                    ((ServerPlayerEntity)player).networkHandler
                        .sendPacket(
                            ModInit.createParticle(amount, this.getX(), this.getY(), this.getZ(), world.random.nextFloat()*0.1f, 0.2F, world.random.nextFloat()*0.1f));
                }
            }
        }
    }

}