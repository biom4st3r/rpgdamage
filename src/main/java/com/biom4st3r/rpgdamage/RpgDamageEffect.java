package com.biom4st3r.rpgdamage;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleType;
import net.minecraft.util.registry.Registry;

public class RpgDamageEffect implements ParticleEffect {
    private static final Factory<RpgDamageEffect> FACTORY = new Factory<RpgDamageEffect>() {
        public RpgDamageEffect read(ParticleType<RpgDamageEffect> pt, StringReader stringReader)
                throws CommandSyntaxException {
            stringReader.expect(' ');
            float damage = (float) stringReader.readDouble();
            stringReader.expect(' ');
            float r = (float) stringReader.readDouble();
            stringReader.expect(' ');
            float g = (float) stringReader.readDouble();
            stringReader.expect(' ');
            float b = (float) stringReader.readDouble();
            return new RpgDamageEffect(damage, r, g, b);
        }

        public RpgDamageEffect read(ParticleType<RpgDamageEffect> pt, PacketByteBuf buf) {
            return new RpgDamageEffect(buf.readFloat(), buf.readFloat(), buf.readFloat(), buf.readFloat());
        }

    };

    public static final ParticleType<RpgDamageEffect> TYPE = new ParticleType<RpgDamageEffect>(true, FACTORY) {

        @Override
        public Codec<RpgDamageEffect> getCodec() {
            return null;
        }
    };
    // ParticleTypes
    public RpgDamageEffect(float value, float red, float green, float blue) {
        this.damage = value;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
    private final float damage;
    public final float red,green,blue;

    public float getValue()
    {
        return damage;
    }

    public ParticleType<RpgDamageEffect> getType() { 
        return TYPE;
    }

    public void write(PacketByteBuf buf) { 
        buf.writeFloat(damage);
        buf.writeFloat(red);
        buf.writeFloat(green);
        buf.writeFloat(blue);
    }

    public String asString() { 
        return String.format("%s %s", Registry.PARTICLE_TYPE.getId(TYPE), this.damage); 
    }
}

